# PyYoloCR-extra

## Notice

The contents of this repository is exclusively aimed at storing the following data:
* Dependencies of the PyYoloCR project, in case any of them become hard or impossible to obtain; specifically for the 64-bit version of Microsoft Windows.
* Tests for the PyYoloCR project.
* Images for PyYoloCR's README